package com.hackaton.customersapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Document("customers")
public class Customer {

    @Id
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String customerId;
    private String firstName;
    private String lastName;
    private String documentNumber;
    private String documentType;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Date creationDate = new Date();

}
