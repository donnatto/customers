package com.hackaton.customersapi.service;

import com.hackaton.customersapi.model.Customer;

import java.util.List;

public interface CustomerService {

    List<Customer> getAllCustomers();
    Customer getCustomerById(String id);
    Customer saveCustomer(Customer customer);
    void updateCustomer(Customer customer, String id);
    void modifyCustomer(Customer customer, String id);
    void deleteCustomer(String id);
}
