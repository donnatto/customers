package com.hackaton.customersapi.utils;

public class Constants {

    private Constants() {
    }

    public static final String BASE_URL = "/customers/v1";
    public static final String CUSTOMERS_URL = BASE_URL + "/customers";
}
