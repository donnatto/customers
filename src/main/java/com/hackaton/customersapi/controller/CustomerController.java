package com.hackaton.customersapi.controller;

import com.hackaton.customersapi.model.Customer;
import com.hackaton.customersapi.service.CustomerService;
import com.hackaton.customersapi.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Constants.CUSTOMERS_URL)
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping
    public ResponseEntity<List<Customer>> getAllCustomers() {
        List<Customer> allCustomers = customerService.getAllCustomers();
        return (allCustomers != null && !allCustomers.isEmpty()) ?
                ResponseEntity.ok(allCustomers) :
                new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{customerId}")
    public ResponseEntity<Customer> getCustomer(@PathVariable String customerId) {
        Customer customer = customerService.getCustomerById(customerId);
        return customer != null ? ResponseEntity.ok(customer) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping
    public ResponseEntity<Customer> saveCustomer(@RequestBody Customer customer) {
        return ResponseEntity.ok(customerService.saveCustomer(customer));
    }

    @PutMapping("/{customerId}")
    public ResponseEntity<Void> updateCustomer(@RequestBody Customer customer, @PathVariable String customerId) {
        if (customerService.getCustomerById(customerId) == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        customerService.updateCustomer(customer, customerId);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/{customerId}")
    public ResponseEntity<Void> modifyCustomer(@RequestBody Customer customer, @PathVariable String customerId) {
        customerService.modifyCustomer(customer, customerId);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{customerId}")
    public ResponseEntity<Void> deletCustomer(@PathVariable String customerId) {
        if (customerService.getCustomerById(customerId) == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        customerService.deleteCustomer(customerId);
        return ResponseEntity.noContent().build();
    }


}
