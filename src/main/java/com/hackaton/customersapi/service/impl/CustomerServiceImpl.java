package com.hackaton.customersapi.service.impl;

import com.hackaton.customersapi.model.Customer;
import com.hackaton.customersapi.repository.CustomerRepository;
import com.hackaton.customersapi.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    @Override
    public Customer getCustomerById(String id) {
        return customerRepository.findById(id).orElse(null);
    }

    @Override
    public Customer saveCustomer(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public void updateCustomer(Customer customer, String id) {
        customer.setCustomerId(id);
        customerRepository.save(customer);
    }

    @Override
    public void modifyCustomer(Customer customer, String id) {
        Customer foundCustomer = customerRepository.findById(id).orElse(null);
        if (foundCustomer == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        String firstName = customer.getFirstName();
        String lastName = customer.getLastName();
        String documentNumber = customer.getDocumentNumber();
        String documentType = customer.getDocumentType();
        if (firstName != null) {
            if (firstName.trim().isEmpty()) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            foundCustomer.setFirstName(firstName);
        }
        if (lastName != null) {
            if (lastName.trim().isEmpty()) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            foundCustomer.setLastName(lastName);
        }
        if (documentNumber != null) {
            if (documentNumber.trim().isEmpty()) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            foundCustomer.setDocumentNumber(documentNumber);
        }
        if (documentType != null) {
            if (documentType.trim().isEmpty()) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            foundCustomer.setDocumentType(documentType);
        }

        customerRepository.save(foundCustomer);
    }

    @Override
    public void deleteCustomer(String id) {
        customerRepository.deleteById(id);
    }
}
